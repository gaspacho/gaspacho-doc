Installation
============

Dependencies
------------

:term:`Gaspacho` is an appication written in python using built-in modules 
as much as possible.


:term:`Gaspacho`'s dependencies are:

* `Python <http://www.python.org/>`_ >= 2.4
* if Python's version is lower than 2.6,
  `SimpleJson <http://undefined.org/python/#simplejson>`_
* `Twisted_Core <http://twistedmatrix.com/>`_ and
  `Twisted_Web <http://twistedmatrix.com/>`_ >= 1.8
* `SQLAlchemy <http://www.sqlalchemy.org/>`_ >= 0.5.2
* `Elixir <http://elixir.ematia.de/>`_ >= 0.6.1
* `ConfigObj <http://www.voidspace.org.uk/python/configobj.html>`_
* `OptParse <http://wiki.python.org/moin/OptParse>`_
* `PAM <http://www.pangalactic.org/PyPAM/>`_ >= 0.5.0
* `formencode <http://formencode.org/>`_


A script is provided to test the whole necessary dependencies::

  python check_dependencies.py

The installation procedure
---------------------------

Download the latest stable version of Gaspacho_.

.. _Gaspacho: http://www.gaspacho-projet.net

Decompress the archive and install it by typing this command::

  python setup.py install

The web application is ordinary installed in the directory 
``/usr/local/share/gaspacho/`` (the default settings of :term:`Gaspacho` uses
this path).

If the files are **not** in the directory, please configure the application
before lauching it.

Then a generated data directory has to be created::

  mkdir -p /var/lib/gaspacho/apply/

Importing the default rules
----------------------------

A default set of rules is proposed by the :term:`Gaspacho`'s scheme.

To import all the rules::

 python import.py -a

First launching
----------------

If the data's directory is the default directory, to launch :term:`Gaspacho`, 
you have to do::

 /usr/bin/twistd -noy /usr/local/share/gaspacho/website.tac

Then go to de :term:`Gaspacho`'s home page whith a modern browser,
`http://localhost:8080/ <http://localhost:8080/>`_ .

The default user name is **admin** with the password **admin**.


