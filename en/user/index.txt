Documentation Gaspacho
===========================================

.. image:: _static/presentation_gaspacho.png
   :scale: 50 
   :alt: connected in Gaspacho
   :align: center 

Content :

.. toctree::
   :maxdepth: 2

   presentation
   concept
   installation
   configuration
   use
   glossary

   
Table of contents
=================

* :ref:`genindex`
* :ref:`search`

