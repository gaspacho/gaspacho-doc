Glossaire
=========

.. glossary::
	
	Gaspacho
		Logiciel de déploiement de configuration sur un parc de machines.

	variable
		Option de configuration élémentaire (par exemple l'image de fond d'écran).

	agent
		Logiciel service tournant sur les machines cibles.

	plugin
		Librairie permettant à un agent de gérer un type de directive.
		(Par exemple une clé de regsitre sous Windows.)

	JSON
		`JSON <http://www.json.org/>`_


